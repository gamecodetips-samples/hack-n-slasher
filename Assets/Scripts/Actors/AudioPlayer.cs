﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Common;
using UnityEngine;

namespace Assets.Scripts.Actors
{
    public class AudioPlayer : MonoBehaviour
    {
        public AudioSource Voice;
        public AudioSource Actions;
        public AudioSource Misc;

        public ClipLibrary[] AudioClips;

        private Dictionary<ClipType, IEnumerable<AudioClip>> clipLibrary;

        void Start()
        {
            clipLibrary = new Dictionary<ClipType, IEnumerable<AudioClip>>();
            foreach (var cl in AudioClips)
                clipLibrary.Add(cl.ClipType, cl.Clips);
        }

        public void PlayVoice(ClipType clipType)
        {
            Voice.clip = clipLibrary[clipType].PickOne();
            Voice.Play();
        }

        public void PlayAction(ClipType clipType)
        {
            Actions.clip = clipLibrary[clipType].PickOne();
            Actions.Play();
        }

        public void PlayMisc(ClipType clipType)
        {
            Misc.clip = clipLibrary[clipType].PickOne();
            Misc.Play();
        }
    }

    [Serializable]
    public class ClipLibrary
    {
        public ClipType ClipType;
        public AudioClip[] Clips;
    }

    public enum ClipType
    {
        Hurt,
        Death,
        Attack,
        Dash,
        WakeUp,
        Impact,
        Magic
    }
}
