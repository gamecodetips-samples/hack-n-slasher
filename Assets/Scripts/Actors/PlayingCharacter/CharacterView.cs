﻿using UnityEngine;

namespace Assets.Scripts.Actors.PlayingCharacter
{
    public class CharacterView : MonoBehaviour
    {
        public Animator Animator;
        public SpriteRenderer Sprite;
        public float AnimationSpeedFactor;
        public AudioPlayer AudioPlayer;
        public VfxPlayer VfxPlayer;

        public void ShowAttack()
        {
            Animator.SetTrigger("Attack" + Random.Range(1, 4));
            AudioPlayer.PlayAction(ClipType.Attack);
        }

        public void ShowMovement(Vector2 movementVector)
        {
            if (movementVector.magnitude > 0)
                Sprite.flipX = movementVector.x < 0;
            Animator.SetFloat("Speed", movementVector.magnitude * AnimationSpeedFactor);
        }

        public void StartRoll()
        {
            Animator.SetBool("Roll", true);
            AudioPlayer.PlayMisc(ClipType.Dash);
            VfxPlayer.PlayTrail();
        }

        public void StopRoll()
        {
            Animator.SetBool("Roll", false);
            VfxPlayer.StopTrail();
        }

        public void ShowHurt(int damage)
        {
            Animator.SetTrigger("Hurt");
            AudioPlayer.PlayVoice(ClipType.Hurt);
            AudioPlayer.PlayAction(ClipType.Impact);
            VfxPlayer.Bleed();
        }

        public void ShowDeath()
        {
            Animator.SetTrigger("Death");
            AudioPlayer.PlayAction(ClipType.Impact);
            AudioPlayer.PlayVoice(ClipType.Death);
            VfxPlayer.Bleed();
        }
    }
}
