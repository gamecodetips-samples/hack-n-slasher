﻿using System.Collections;
using Assets.Scripts.Actions;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class BossAi : BaseAi
    {
        public float ProximityThreshold;
        public float AttackFrequency;
        public float FrequencyModifier;
        public float BaseSpecialInterval;
        public float SpecialIntervalModifier;
        public ShapedArea SpecialAttackArea;
        public int SpecialAttackDamage;

        private float SpecialAttackInterval =>
            BaseSpecialInterval + Random.Range(-SpecialIntervalModifier, SpecialIntervalModifier);

        public override void Initialize(ActorProvider actorProvider, ActorStats stats, MonsterMovement movement, MonsterView view)
        {
            base.Initialize(actorProvider, stats, movement, view);
            this.AnimationEvents?.Initialize(OnSwing, OnSpecialCast);
        }

        private IEnumerator WaitForSpecialAttack()
        {
            yield return new WaitForSeconds(SpecialAttackInterval);
            CastSpecialAttack();
        }

        private float AttackInterval => AttackFrequency + Random.Range(-FrequencyModifier, FrequencyModifier);

        private bool IsFarFromHero =>
            Vector3.Distance(transform.position, hero.Position) > ProximityThreshold;
        
        void Update()
        {
            if (!isPerformingAction)
            {
                if (IsFarFromHero)
                    movement.MoveTowards(hero.Position);
                else
                    Attack();
            }
        }

        private void Attack()
        {
            movement.MoveTowards(hero.Position);
            Cone.LookAt(hero.Position);
            isPerformingAction = true;
            view.ShowAttack();
            StartCoroutine(WaitForNextAttack());
        }

        private IEnumerator WaitForNextAttack()
        {
            yield return new WaitForSeconds(AttackInterval);
            isPerformingAction = false;
        }

        public override void Enable()
        {
            base.Enable();
            StartCoroutine(WaitForSpecialAttack());
        }

        private void CastSpecialAttack()
        {
            StopAllCoroutines();
            isPerformingAction = true;
            movement.Stop();
            view.ShowSpecialCast();
            StartCoroutine(WaitForNextAttack());
            StartCoroutine(WaitForSpecialAttack());
        }

        private void OnSpecialCast()
        {
            view.ShowSpecialAttack();
            if (SpecialAttackArea.IsInArea(hero.Position))
                hero.ReceiveDamage(SpecialAttackDamage);
        }
    }
}
