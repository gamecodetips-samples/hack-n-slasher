﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class ChaseAi : BaseAi
    {
        public float ProximityThreshold;
        public float AttackFrequency;
        public float FrequencyModifier;

        private float attackInterval => AttackFrequency + Random.Range(-FrequencyModifier, FrequencyModifier);

        private bool IsFarFromHero =>
            Vector3.Distance(transform.position, hero.Position) > ProximityThreshold;
        
        void Update()
        {
            if (!isPerformingAction)
            {
                if (IsFarFromHero)
                    movement.MoveTowards(hero.Position);
                else
                    Attack();
            }
        }

        private void Attack()
        {
            movement.MoveTowards(hero.Position);
            Cone.LookAt(hero.Position);
            isPerformingAction = true;
            view.ShowAttack();
            StartCoroutine(WaitForNextAttack());
        }

        private IEnumerator WaitForNextAttack()
        {
            yield return new WaitForSeconds(attackInterval);
            isPerformingAction = false;
        }
    }
}
