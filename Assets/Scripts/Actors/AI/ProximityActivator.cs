﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Actors.PlayingCharacter;
using Assets.Scripts.Directors;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class ProximityActivator : MonoBehaviour
    {
        public float ActivationDistance;

        private Character hero;
        private BaseAi ai;
        private MonsterView view;

        private bool HeroInRange => Vector3.Distance(transform.position, hero.Position) < ActivationDistance;

        public void Initialize(ActorProvider actorProvider, BaseAi ai, MonsterView view)
        {
            this.view = view;
            this.ai = ai;
            this.hero = actorProvider.Hero;

            ai.Disable();
        }

        void Update()
        {
            if (HeroInRange)
            {
                ai.Enable();
                view.WakeUp();
                enabled = false;
            }
        }

        void OnDrawGizmos() => Handles.DrawWireDisc(transform.position, Vector3.up, ActivationDistance);
    }
}
