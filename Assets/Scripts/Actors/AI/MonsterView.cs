﻿using Assets.Scripts.Actors.Status;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class MonsterView : MonoBehaviour
    {
        public Animator Animator;
        public StatusDisplay StatusDisplay;
        public SpriteRenderer Sprite;
        public AudioPlayer AudioPlayer;
        public VfxPlayer VfxPlayer;

        public bool FlipFix;
        private int painTolerance;

        public void Initialize(int painTolerance) => 
            this.painTolerance = painTolerance;

        public void ShowHurt(int damage)
        {
            StatusDisplay.ShowDamage(damage);
            AudioPlayer.PlayAction(ClipType.Impact);
            VfxPlayer.Bleed();

            if (damage > painTolerance)
            {
                Animator.SetTrigger("Hurt");
                AudioPlayer.PlayVoice(ClipType.Hurt);
            }
        }
        
        public void ShowMovement(Vector3 movementVector)
        {
            if (movementVector.magnitude > 0)
            {
                Sprite.flipX = movementVector.x < 0;
                if (FlipFix)
                    Sprite.flipX = !Sprite.flipX;
            }

            Animator.SetFloat("Speed", movementVector.magnitude * 100f);
        }

        public void ShowAttack()
        {
            StopMoving();
            Animator.SetTrigger("Attack");
            AudioPlayer.PlayAction(ClipType.Attack);
        }

        public void ShowDeath()
        {
            StopMoving();
            Animator.SetTrigger("Death");
            AudioPlayer.PlayVoice(ClipType.Death);
            AudioPlayer.PlayAction(ClipType.Impact);
            VfxPlayer.Bleed();
        }

        public void StopMoving() => 
            Animator.SetFloat("Speed", 0);

        public void WakeUp() => 
            AudioPlayer.PlayVoice(ClipType.WakeUp);

        public void ShowSpecialAttack()
        {
            VfxPlayer.PlaySpecial();
            AudioPlayer.PlayAction(ClipType.Magic);
        }

        public void ShowSpecialCast() => 
            Animator.SetTrigger("Special");
    }
}