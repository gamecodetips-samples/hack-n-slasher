﻿using Assets.Scripts.Actions;
using UnityEngine;

namespace Assets.Scripts.Actors.AI
{
    public class DiveAttack : MonoBehaviour
    {
        public float Duration;
        public float Speed;
        private MonsterMovement movement;

        private float elapsed;
        private MonsterView view;
        private Vector3 direction;
        private CollisionDetection detector;

        public void Initialize(CollisionDetection detector, ConeArea cone, MonsterView view)
        {
            this.detector = detector;
            this.view = view;
            enabled = false;
        }

        public void Do(Vector3 direction)
        {
            this.direction = direction.normalized;
            view.ShowAttack();
            elapsed = Duration;
            enabled = true;
        }

        void Update()
        {
            var moveVector = direction * Speed * Time.deltaTime;
            if (detector.CanMoveTowards(moveVector))
                transform.position += moveVector;

            view.ShowMovement(moveVector);
            elapsed -= Time.deltaTime;
            if(elapsed <= 0)
                OnDashFinised();
        }

        void OnDashFinised()
        {
            enabled = false;
        }
    }
}
