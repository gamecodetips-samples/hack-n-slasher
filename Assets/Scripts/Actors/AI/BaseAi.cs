﻿using Assets.Scripts.Actions;
using Assets.Scripts.Actors.PlayingCharacter;
using Assets.Scripts.Directors;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Actors.AI
{
    public abstract class BaseAi : MonoBehaviour
    {
        public ConeArea Cone;
        public AnimationEvents AnimationEvents;

        protected bool isPerformingAction;
        protected Character hero;
        protected MonsterMovement movement;
        protected MonsterView view;
        protected ActorStats stats;

        public virtual void Initialize(ActorProvider actorProvider, ActorStats stats, MonsterMovement movement,
            MonsterView view)
        {
            this.hero = actorProvider.Hero;
            this.movement = movement;
            this.view = view;
            this.stats = stats;
            this.AnimationEvents?.Initialize(OnSwing);
        }

        protected virtual void OnSwing()
        {
            if (Cone.IsInArea(hero.Position))
                hero.ReceiveDamage(Random.Range(stats.MinDamage, stats.MaxDamage));
        }

        public void Disable()
        {
            StopAllCoroutines();
            enabled = false;
        }

        public virtual void Enable()
        {
            isPerformingAction = false;
            enabled = true;
        }
    }
}
