﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Actors.AI;
using UnityEngine;

namespace Assets.Scripts.Directors
{
    public class MonsterInitializer : MonoBehaviour
    {
        public ActorProvider ActorProvider;
        private IEnumerable<Monster> monsters;

        void Start()
        {
            monsters = GetComponentsInChildren<Monster>();
            ActorProvider.Initialize(monsters);

            foreach (var m in monsters)
                m.Initialize(ActorProvider);
        }
    }
}
