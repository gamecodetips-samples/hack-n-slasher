﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Tether : MonoBehaviour
    {
        public float Elasticity;
        public Transform Target;

        void Update() => 
            transform.position = Vector3.Lerp(transform.position, Target.position, Elasticity);
    }
}
