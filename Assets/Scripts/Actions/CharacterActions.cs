﻿using System.Collections;
using Assets.Scripts.Actors;
using Assets.Scripts.Actors.PlayingCharacter;
using Assets.Scripts.Directors;
using UnityEngine;

namespace Assets.Scripts.Actions
{
    public class CharacterActions : MonoBehaviour
    {
        public float AttackTime;
        public ConeArea Area;

        private CharacterView view;
        private CharacterMovement movement;
        private bool performingAction = false;
        private Dash dash;
        private ActorProvider actorProvider;
        private ActorStats stats;

        private bool CanAttack => !dash.enabled && !performingAction;

        public void Initialize(ActorStats stats, CharacterMovement movement, Dash dash, CharacterView view,
            ActorProvider actorProvider)
        {
            this.stats = stats;
            this.actorProvider = actorProvider;
            this.view = view;
            this.movement = movement;
            this.dash = dash;
        }

        public void Attack()
        {
            if (CanAttack)
            {
                view.ShowAttack();
                HitTargetsInArea();
                StartCoroutine(PreventMovementDuringAttack());
            }
        }

        private void HitTargetsInArea()
        {
            var hits = actorProvider.GetMonstersInArea(Area);
            foreach (var monster in hits)
            {
                var damage = Random.Range(stats.MinDamage, stats.MaxDamage);
                monster.ReceiveDamage(damage);
            }
        }

        IEnumerator PreventMovementDuringAttack()
        {
            movement.Disable();
            performingAction = true;
            yield return new WaitForSeconds(AttackTime);
            performingAction = false;
            movement.Enable();
        }

        public void Dash(Vector3 direction) => dash.Do(direction);

        public void SetAttackDirectionTo(Vector2 inputVector)
        {
            var lookAtVector = new Vector3(inputVector.x, 0f, inputVector.y).normalized;
            Area.LookAt(transform.position + lookAtVector);
        }
    }
}